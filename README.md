VanDamBaam
==========

Jean Claude Van Damme remplit petit shot gun musclé

Le jeu est un launch game qui met en scene JCVD.
Le joueur lance JCVD à l'aide de la barre espace (jauge d'angle (jauge circulaire) puis puissance (jauge droite) réglables).
JCVD possede un petit shotgun qui tire un nombre limité de cartouches d'un coté, et des petits JCVD de l'autre.

Une fois lancé, JCVD essaie d'atteindre la plus grande distance possible et de marquer le plus de points en détruisant les obstacles sur sa route.

Le joueur peut:
- détruire les obstacles rencontrés avec une cartouche de shotgun (fleche droite)
- reprendre de l'élan au sol en tirant une cartouche vers le bas (fleche bas)
- tirer un plus petit et leger JCVD (maintien appui sur espace pour lancer la jauge puis relacher pour selectionner l'angle)


Au contact d'un obstacle, JCVD est ralenti jusqu'à toucher le sol.
Le contact avec le sol ralentit JCVD par frottement, puis JCVD rebondit. JCVD continue à avancer.

Le jeu s'arrete lorsque JCVD est à l'arret (V<Vmin) au sol.

L écran affiche la distance parcourue par le joueur, la distance max, la vitesse, la hauteur.

La camera reste centrée sur JCVD ; si JCVD est en train de monter, il se retrouve dans la partie supérieure de l ecran et s il descend il se rapproche du bas.
