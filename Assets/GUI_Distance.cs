﻿using UnityEngine;
using System.Collections;

public class GUI_Distance : MonoBehaviour {
	
	public gameCtrl game;
	public Transform startPosition;
	public float unityToMeter=1f;
	public GUIStyle style;
	public Vector2 adjustement;
	
	void OnGUI () {
		if (game.powerIntensity < 1f)
		{
			Vector2 pos = new Vector2((Screen.width)-adjustement.x, Screen.height - adjustement.y);
			GUI.Label(new Rect(pos.x, pos.y, 204, 24),""+((int)GetDistance())+" m",style);
		}
	}
	
	public float  GetDistance ()
	{
		VanDamme van = VanDamme.lastVanDamme;
		if (startPosition != null && van != null) {
			Vector2 start = new Vector2 (startPosition.position.x, 0f);
			Vector2 vandame = new Vector2 (van.transform.position.x, 0f);
			return Vector2.Distance (start, vandame)*unityToMeter;
		}
		return 0f;
	}
}
