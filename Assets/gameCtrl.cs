﻿using UnityEngine;
using System.Collections;

public class gameCtrl : MonoBehaviour {
    static public gameCtrl game;
    public int points;

    bool powerActive;
    public float powerIntensity;
    bool powerIncrease;

    public int ammo;
    public int subLives;
    public int maxSubLives;

    public VanDamme player;
    Rigidbody2D playerRigid;

    public ChuckLegCtrl chuck;
    public float launchAngle;

    public Transform startPosition;
        
	// Use this for initialization
	void Start () {
        points = 0;
        powerActive = false;
        powerIntensity = 0f;
        powerIncrease = true;

        ammo = 2;
        subLives = 8;
        maxSubLives = 8;
        game = this;
	}
	
	// Update is called once per frame
	void Update () {
        if (powerActive)
        {
            if (powerIncrease)
                powerIntensity += Mathf.Log(powerIntensity, 12);
            else
                powerIntensity -= Mathf.Log(powerIntensity, 12);
            if (powerIntensity < 2)
            {
                powerIntensity = 2f;
                powerIncrease = true;
            }
            else if (powerIntensity >= 100)
            {
                powerIncrease = false;
            }
        }

        if (player)
            playerRigid = player.GetComponent<Rigidbody2D>();
        if (ammo == 0 && subLives == 0 && playerRigid.velocity.x == 0)
        {
            points += (int)GetDistance();
            PlayerPrefs.SetInt("currScore", points);
            Application.LoadLevel("gameOver");
        }
	}
    public float GetDistance()
    {
        VanDamme van = VanDamme.lastVanDamme;
        if (startPosition != null && van != null)
        {
            Vector2 start = new Vector2(startPosition.position.x, 0f);
            Vector2 vandame = new Vector2(van.transform.position.x, 0f);
            return Vector2.Distance(start, vandame);
        }
        return 0f;
    }
    public bool launchStep(){
        if (!chuck.isRotating() && !powerActive)
        {
            chuck.startRotation();
            return false;
        }
        else if (!powerActive)
        {
            launchAngle = chuck.stopRotation();
            if (launchAngle > 360)
                launchAngle -= 360;
            powerActive = true;
            return false;
        }
        else
        {
            stopPower();
            audio.Play();
            return true;
        }
         

    }
    public void stopPower()
    {
        powerActive = false;
        //send current PowerIntensity value
        Invoke("resetPower", 1);
    }
    void resetPower()
    {
        powerIntensity = 0f;
    }
    public void popClone()
    {
        subLives -= 1;
        ammo = 2;
    }
}
