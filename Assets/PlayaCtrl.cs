﻿using UnityEngine;
using System.Collections;

public class PlayaCtrl : MonoBehaviour {

    protected Animator animator;
    protected Transform transf;
    protected Rigidbody2D body;
    public int health;

	void Start () {
        animator = GetComponent<Animator>();
        transf = GetComponent<Transform>();
        body = GetComponent<Rigidbody2D>();
        health = 100;
    }
	
	void Update () {
        if (animator)
        {
            if (transform.position.y > 1)
                animator.SetBool("flying", true);
            else
                animator.SetBool("flying", false);
        }
        if (Input.GetKey(KeyCode.UpArrow))
            body.AddForce(new Vector2(2, 0));
        else if (Input.GetKey(KeyCode.LeftArrow))
            health -= 1;
	}
}
