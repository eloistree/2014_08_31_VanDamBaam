﻿using UnityEngine;
using System.Collections;

public class BulletFactory : MonoBehaviour {


	public static BulletFactory instance;
	public GameObject shootInFrontPrefab;
	public GameObject shootDownPrefab;

	void Start () {
	
		if(instance==null)
			instance = this;
	}

	public static void ShotDown(Vector3 where)
	{
		if (instance == null) {
			Debug.LogWarning("No facotry instancied in the scene");return;}
		if (instance .shootDownPrefab== null) {
			Debug.LogWarning("No prefab linked",instance);return;}
		GameObject.Instantiate(instance.shootDownPrefab,where, new Quaternion());
	}

	public static void ShotFront (Vector3 where)
	{	if (instance == null) {
			Debug.LogWarning("No facotry instancied in the scene");return;}
		if (instance .shootDownPrefab== null) {
			Debug.LogWarning("No prefab linked",instance);return;}
		GameObject.Instantiate(instance.shootInFrontPrefab,where, new Quaternion());
	}
}
