﻿using UnityEngine;
using System.Collections;

public class VanStartFactory : MonoBehaviour {

	public Transform popPoint;
	public GameObject [] vanDammesPrefab;
	public CameraFollow mainCameraFollower;
	public gameCtrl game;


	void Start () {
		if (vanDammesPrefab == null || vanDammesPrefab.Length < 1 || popPoint==null ||mainCameraFollower==null || game==null)
						Debug.Log ("No, please no");
		int randomSelect = Random.Range (0, vanDammesPrefab.Length );
		GameObject selected = vanDammesPrefab [randomSelect];
		GameObject createdVan = GameObject.Instantiate (selected,popPoint.position, new Quaternion() ) as GameObject;
		VanDamme vandammeCreated = createdVan.GetComponent<VanDamme> () as VanDamme;

		if (vandammeCreated != null) {
			vandammeCreated.cameraScript = mainCameraFollower;
			vandammeCreated.game = game;
			mainCameraFollower.target= createdVan.transform;

		}


	}
	

}
