﻿using UnityEngine;
using System.Collections;

public class CloneJump : MonoBehaviour {
	

	public VanDamme vanDamme;
	public bool hasBeenUsed;

	public Vector2 direction;
	public float angle=45;
	public float maxAngle=80;
	public float minAngle=-80;
	public float powerPourcent=1f;
	public float maxPower=10f;

	public float sizeDownByClone=0.05f;
	public GameObject vanDammePrefab;
	public Vector3 apparitionAdjustment;
	public float originalSpeedReduceBy=2f;


	public KeyCode jumpKeyCode= KeyCode.C;
	
	void Start()
	{
		
		RefreshDirectionWithAngle ();
	}
	void Update()
	{
		if (hasBeenUsed)
						return;

		if(Input.GetAxis ("Vertical")!=0f)
		{
			//angle += Input.GetAxis ("Vertical");
			//RefreshDirectionWithAngle ();
		}

	}
	
	void RefreshDirectionWithAngle ()
	{
		angle = Mathf.Clamp (angle, minAngle, maxAngle);
		direction.x = Mathf.Cos (angle * Mathf.Deg2Rad);
		direction.y = Mathf.Sin (angle * Mathf.Deg2Rad);
	}
	
	public void JumpAndClone()
	{

		if (vanDammePrefab != null && vanDamme != null) {
			Vector3 posWithAdjustment = new Vector3(this.transform.position.x+apparitionAdjustment.x,
			                                        this.transform.position.y+apparitionAdjustment.y,
			                                        this.transform.position.z+apparitionAdjustment.z);
			GameObject vanDamClone = Instantiate (vanDammePrefab, posWithAdjustment, this.transform.rotation) as GameObject;
			float size = 1f-sizeDownByClone*(float)(vanDamme.game.maxSubLives - vanDamme.game.subLives);
						vanDamClone.transform.localScale=new Vector3(size,size,size);
			
						vanDamClone.rigidbody2D.velocity = new Vector2 (vanDamClone.rigidbody2D.velocity.x, 0f);
						vanDamClone.rigidbody2D.AddForce (direction.normalized * (maxPower * powerPourcent));
						VanDamme vdRef = vanDamClone.GetComponent<VanDamme>();
						vdRef.RequireCameraFocus();

                        vanDamme.game.player = vdRef;
						
					vanDamme.rigidbody2D.velocity /=originalSpeedReduceBy; 
						vanDamme.Deactivate ();
						vanDamme.AutoDestruction (10f);
						hasBeenUsed = true;
						Destroy (this.gameObject,10f);
				} else {
			Debug.LogWarning("All data are not fielded, see vanDamme ref and prefab",this.gameObject);		
		}



	}
}