﻿using UnityEngine;
using System.Collections;

public class JumpImpulsion : MonoBehaviour {

	public float power=100f;


	public void Jump()
	{
		this.rigidbody2D.velocity = new Vector2 (this.rigidbody2D.velocity.x, 0f);
		this.rigidbody2D.AddForce (Vector2.up*power);
	}
}
