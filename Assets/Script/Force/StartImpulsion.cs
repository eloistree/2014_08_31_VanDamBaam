﻿using UnityEngine;
using System.Collections;

public class StartImpulsion : MonoBehaviour {

	public Vector2 direction;
	public float angle=45;
	public float maxPower=10f;

	void Start()
	{
		
		RefreshDirectionWithAngle ();
	}
	void Update()
	{

		if(Input.GetAxis ("Vertical")!=0f)
		{
			angle += Input.GetAxis ("Vertical");
			RefreshDirectionWithAngle ();
		}
	}

	void RefreshDirectionWithAngle ()
	{
		angle = Mathf.Clamp (angle, 0f, 89f);
		direction.x = Mathf.Cos (angle * Mathf.Deg2Rad);
		direction.y = Mathf.Sin (angle * Mathf.Deg2Rad);
	}

	public void Jump(float intensity, float ang)
	{
        angle = ang;
        RefreshDirectionWithAngle();
        this.rigidbody2D.AddForce(direction.normalized * (maxPower * (intensity/100)));
		Destroy (this);
	}
}
