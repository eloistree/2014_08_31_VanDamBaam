﻿using UnityEngine;
using System.Collections;

public class VanDamme : MonoBehaviour {

	public static VanDamme lastVanDamme;
    public gameCtrl game;

	public static int jumpCount=0;
	
	public StartImpulsion startJump;
	public bool hasStartJumped;
	public JumpImpulsion classicJump;
	public CloneJump cloneJump;
	public CameraFollow cameraScript;
	
	public Transform frontShotPosition;
	public Transform downShotPosition;
    	
	public VanDammeState vanState;
	public GameObject toActivateAtStart;

	public bool deactivatedInput;

    AudioSource shotgun;
    AudioSource clone;
    AudioSource hit;
	void Start () {
		lastVanDamme = this;
        game.popClone();
        var aSources = GetComponents<AudioSource>();
        shotgun = aSources[0];
//        clone = aSources[1];
  //      hit = aSources[2];
	}
	
	// Update is called once per frame
	void Update () {
	
		InputListener ();
	}

	void InputListener ()
	{
		if (deactivatedInput)
						return;
		if ( game.subLives > 0 &&  cloneJump!=null && Input.GetKeyDown (KeyCode.UpArrow)) {
			cloneJump.JumpAndClone();
			vanState.ChangeState(VanDammeState.VanPositionType.CloneFire);
            shotgun.Play();
		}
		
		if (!hasStartJumped && startJump!=null && Input.GetButtonDown ("Jump")) {
            //game.stopPower();
            if (game.launchStep())
            {
                hasStartJumped = true;
                var obj = GameObject.Find("CanonPoint");
                if (obj)
                    transform.position = obj.transform.position;
                else
                    Debug.Log("Failed to position JCVD at chucks leg");
				var ob = toActivateAtStart;
                if (ob)
                    ob.gameObject.SetActive(true);
                startJump.Jump(game.powerIntensity, game.launchAngle);
            }
		}
		else if (hasStartJumped && game.ammo>0 && classicJump!=null && Input.GetKeyDown(KeyCode.DownArrow)) {
			classicJump.Jump();
			vanState.ChangeState(VanDammeState.VanPositionType.DownFire);
            shotgun.Play();
			game.ammo--;
			BulletFactory.ShotDown(downShotPosition.position);

		}

		if (game.ammo > 0 && Input.GetKeyDown(KeyCode.RightArrow) && hasStartJumped) {
			game.ammo--;
			vanState.ChangeState(VanDammeState.VanPositionType.FrontFire);
            shotgun.Play();
			BulletFactory.ShotFront(frontShotPosition.position);

		}

	}

	public void Deactivate ()
	{
		deactivatedInput = true;


	}

	public void AutoDestruction (float time)
	{
		Destroy (this.gameObject,time);

	}

	public void RequireCameraFocus ()
	{
		cameraScript.target = this.transform;
	}

	public static void AddAmmo (int ammo)
	{
		if(lastVanDamme!=null && lastVanDamme.game!=null)
		{
			lastVanDamme.game.ammo+=ammo;
		}
	}
}
