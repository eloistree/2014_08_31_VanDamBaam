﻿using UnityEngine;
using System.Collections;

public class VanDammeState : MonoBehaviour {

	public enum VanPositionType{FrontFire, DownFire, CloneFire, Fire }
	public float stateCountDown;
	public GameObject front;
	public float frontStayTime=100;
	public GameObject down;
	public float downStayTime=2;
	public GameObject clone;
	public float cloneStayTime=2;
	public GameObject fire;
	public float fireStayTime=2;

	void Start () {
		ChangeState (VanPositionType.Fire);
	}

	void Update () {
		stateCountDown -= Time.deltaTime;
		if (stateCountDown < 0)
						ChangeState (VanPositionType.FrontFire);
	}

	public void ChangeState(VanPositionType posType)
	{
		
		fire.SetActive(false);
		clone.SetActive(false);
		down.SetActive(false);
		front.SetActive(false);

		float stayTime = 10f;
		switch (posType) {
		case VanPositionType.Fire: 
			stayTime= fireStayTime;
			fire.SetActive(true);
			break;
		case VanPositionType.CloneFire:
			stayTime= cloneStayTime;
			clone.SetActive(true);
			break;
		case VanPositionType.DownFire:
			stayTime= downStayTime;
			down.SetActive(true);
			break;
		case VanPositionType.FrontFire:
			stayTime= frontStayTime;
			front.SetActive(true);
			break;		
		}
		stateCountDown = stayTime;




	}
}
