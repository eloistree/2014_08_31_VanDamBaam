﻿using UnityEngine;
using System.Collections;

public class WallScript : MonoBehaviour {

	public float speedSlowPourcentage=0.1f;
	public GameObject[] wallblocks;
	public Transform explosionCenter;
	public float power=400;
	public float explositionRadius=20;

	public void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag.Equals ("Player") || col.gameObject.tag.Equals ("Bullet")) {
			col.gameObject.rigidbody2D.velocity*=1f-speedSlowPourcentage;
			DestroyWall (col.gameObject.transform.position);
			if(col.gameObject.tag.Equals ("Bullet"))
			{ Destroy(col.gameObject);}
		}
	}

	void DestroyWall (Vector3 explosionPosition)
	{
		foreach (GameObject blocks in wallblocks) {
			if(blocks!=null && blocks.rigidbody!=null)
			{
				blocks.rigidbody.useGravity=true;
				blocks.rigidbody.AddExplosionForce(power,explosionPosition,explositionRadius);
			}
		}
		Destroy (this);
	}
}
