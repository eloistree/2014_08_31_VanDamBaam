﻿using UnityEngine;
using System.Collections;

public class AmmoPack : MonoBehaviour {

	public int ammo=4;

	public void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag.Equals ("Player")) {
			VanDamme.AddAmmo(ammo);		
			Destroy(this.gameObject);
		}
	}
}
