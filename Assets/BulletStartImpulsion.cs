﻿using UnityEngine;
using System.Collections;

public class BulletStartImpulsion : MonoBehaviour {
	
	public Vector2 direction = Vector2.right;
	public float force=100f;
	public float lifeTime=10f;
	void Start () {
		this.rigidbody2D.AddForce (direction * force);
		Destroy (this.gameObject,lifeTime);
	}

}
