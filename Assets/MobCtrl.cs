﻿using UnityEngine;
using System.Collections;

public class MobCtrl : MonoBehaviour
{
    public float speedSlowPourcentage = 0.1f;
    public int speed = 10;
    public int distance = 30;
    public bool vertical = false;
    bool direction = true;

    Vector2 startPos;
    Rigidbody2D body;

	// Use this for initialization
	void Start () {
        startPos = transform.position;
        body = GetComponent<Rigidbody2D>();
        if (vertical)
        {
            if (direction)
                body.AddForce(new Vector2(0, speed));
            else
                body.AddForce(new Vector2(0, -speed));
        }
        else
        {
            if (direction)
                body.AddForce(new Vector2(speed, 0));
            else
                body.AddForce(new Vector2(-speed, 0));
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (Vector2.Distance(startPos, transform.position) > distance)
        {
            direction = !direction;
            body.velocity = new Vector2(0, 0);
            if (vertical)
            {
                if (direction)
                    body.AddForce(new Vector2(0, speed));
                else
                    body.AddForce(new Vector2(0, -speed));
            }
            else
            {
                if (direction)
                    body.AddForce(new Vector2(speed, 0));
                else
                    body.AddForce(new Vector2(-speed, 0));
            }
        }
	}
    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("Player") || col.gameObject.tag.Equals("Bullet"))
        {
            col.gameObject.rigidbody2D.velocity *= 1f - speedSlowPourcentage;
            if (col.gameObject.tag.Equals("Bullet"))
            {
                Destroy(col.gameObject);
                gameCtrl.game.points += 250;
                audio.Play();
                Destroy(this.gameObject);
            }
        }
        Debug.Log("touch");
            direction = !direction;
    }
}
