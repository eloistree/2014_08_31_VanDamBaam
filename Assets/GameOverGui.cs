﻿using UnityEngine;
using System.Collections;

public class GameOverGui : MonoBehaviour {
    gameCtrl game;
    public GUIStyle style;

    bool highscore, best;

    int first, second, third, current;

    string quote;
    private string textFieldString = "";
    void Start()
    {
        highscore = false;
        best = false;
        first = PlayerPrefs.GetInt("firstScore");
        second = PlayerPrefs.GetInt("secondScore");
        third = PlayerPrefs.GetInt("thirdScore");
        current = PlayerPrefs.GetInt("currScore");

        if (current >= first)
            best = true;
        if (current >= third)
            highscore = true;

        quote = Quotes.quotes[Random.Range(0, Quotes.quotes.Length - 1)];
    }
    void OnGUI () {
        //quote
        GUI.Label(new Rect((Screen.width/2)-250, Screen.height-80, 500, 80), quote, style);
        GUI.Box(new Rect(Screen.width-300, (Screen.height/2) -120, 280, 220), "GAME OVER, YOU SUCK");

        string str;
        if(highscore)
            str = "Your Score : " + current.ToString();
        else
            str = "Your Shitty Score : " + current.ToString();
        GUI.Label(new Rect(Screen.width - 260, (Screen.height / 2) - 100, 200, 40), 
            str, style);

        GUI.Label(new Rect(Screen.width - 260, (Screen.height / 2) - 40, 200, 40),
            "JCVD: " + first.ToString(), style);
        GUI.Label(new Rect(Screen.width - 260, (Screen.height / 2) -20, 200, 40),
            "JCVD: " + second.ToString(), style);
        GUI.Label(new Rect(Screen.width - 260, (Screen.height / 2), 200, 40),
            "JCVD: " + third.ToString(), style);

        if (highscore)
        {
            string JCVD = "JCVD";
            GUI.Label(new Rect(Screen.width - 300, (Screen.height / 2) - 75, 200, 40), "New HighScore, enter initials :", style);

            if (textFieldString != "JCVD")
            {
                textFieldString = GUI.TextField(new Rect(Screen.width - 100, (Screen.height / 2) - 70, 50, 30), textFieldString);

                if (textFieldString.Length > 4)
                {
                    textFieldString = JCVD;
                }
                else
                {
                    var working = textFieldString.ToCharArray();
                    for (int i = 0; i < textFieldString.Length; i++)
                    {
                        if (textFieldString[i] != JCVD[i])
                            working[i] = JCVD[i];
                    }
                    textFieldString = new string(working);
                }
            }
            else
                if (GUI.Button(new Rect(Screen.width - 100, (Screen.height / 2) - 70, 50, 30), "JCVD"))
                {
                    saveScore();
                    highscore = false;
                }
        }

        if (GUI.Button(new Rect(Screen.width - 130, (Screen.height / 2) + 40, 80, 40), "Try again"))
        {
            Application.LoadLevel(1);
        }
        if (best)
        {
            if (GUI.Button(new Rect(Screen.width - 270, (Screen.height / 2) + 40, 80, 40), "Proudly exit"))
            {
                Application.LoadLevel(0);
            }
        }
        
	}
    void saveScore()
    {
        if (current > first)
        {
            PlayerPrefs.SetInt("firstScore", current);
            PlayerPrefs.SetInt("secondScore", first);
            PlayerPrefs.SetInt("thirdScore", second);
            best = true;
        }
        else if (current > second)
        {
            PlayerPrefs.SetInt("secondScore", current);
            PlayerPrefs.SetInt("thirdScore", second);
        }
        else if (current > third)
        {
            PlayerPrefs.SetInt("thirdScore", current);
        }

        first = PlayerPrefs.GetInt("firstScore");
        second = PlayerPrefs.GetInt("secondScore");
        third = PlayerPrefs.GetInt("thirdScore");
    }
}
